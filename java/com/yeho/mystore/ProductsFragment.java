package com.yeho.mystore;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import com.yeho.mystore.adapters.AdapterProducts;
import com.yeho.mystore.entidades.Products;
import com.yeho.mystore.unicos.DatosUsuarioUnico;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductsFragment extends Fragment {

  GridView gridProducts = null;
  Context contexto = null;
  private static final String TAG = "ProductsFragment";
  Toolbar toolbar = null;
  DatosUsuarioUnico datosusuario = DatosUsuarioUnico.obtenerInstancia();

  public ProductsFragment() {
    // Required empty public constructor
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);

  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View v= null;
    try {

    contexto = getActivity();
      LayoutInflater gridInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      v = gridInflater.inflate(R.layout.fragment_products, null);

      gridProducts = (GridView) v.findViewById(R.id.gridProducts);

      toolbar = datosusuario.getToolbar();
      //TODO obtener la lista de productos
      if (datosusuario.getListaProductos() == null) {
        agregaProductosDummy();
      }
      gridProducts.setAdapter(new AdapterProducts(contexto, ProductsFragment.this));


    } catch (Exception e) {
      Log.e(TAG, "onCreateView: " + e.getMessage() );
    }

    return v;
  }

  @Override public void onResume() {
    super.onResume();
    if (datosusuario.getListaProductos() != null) {
      gridProducts.setAdapter(new AdapterProducts(contexto, ProductsFragment.this));
    }
  }

  private void agregaProductosDummy() {

    ArrayList<Products> productosLista = new ArrayList<>();

    Products producto = new Products();
    producto.setId("0");
    producto.setName("Campbell-C");
    producto.setPrice("$ 120.00");
    producto.setStock("15");
    producto.setAvailable_date("Jan 29, 2017");
    producto.setDescription("lentes grises");
    producto.setShipping("Envio aereo, terrestre, maritimo");
    producto.setImage("campbell_picture_1");
    Products producto2 = new Products();
    producto2.setId("1");
    producto2.setName("Evans-R");
    producto2.setPrice("$ 115.00");
    producto2.setStock("23");
    producto2.setAvailable_date("Feb 15, 2017");
    producto2.setDescription("lentes verde");
    producto2.setShipping("Envio aereo, terrestre, maritimo");
    producto2.setImage("evans_picture_1");
    Products producto3 = new Products();
    producto3.setId("2");
    producto3.setName("Morgan-L");
    producto3.setPrice("$ 136.00");
    producto3.setStock("19");
    producto3.setAvailable_date("Jan 20, 2017");
    producto3.setDescription("lentes dorados");
    producto3.setShipping("Envio aereo, terrestre, maritimo");
    producto3.setImage("morgan_picture_1");
    Products producto4 = new Products();
    producto4.setId("3");
    producto4.setName("Robinson-A");
    producto4.setPrice("$ 118.00");
    producto4.setStock("11");
    producto4.setAvailable_date("Jan 1, 2017");
    producto4.setDescription("lentes azules");
    producto4.setShipping("Envio aereo, terrestre, maritimo");
    producto4.setImage("robinson_picture_1");

    productosLista.add(producto);
    productosLista.add(producto2);
    productosLista.add(producto3);
    productosLista.add(producto4);

    datosusuario.setListaProductos(productosLista);
  }

  public void callDetailsProduct(String idProducto) {
    //llamar al details
    try {
      final FragmentTransaction ft = getFragmentManager().beginTransaction();
      ft.replace(R.id.frame, new ProductDetailFragment(idProducto));
      ft.commit();
      toolbar.setTitle("PRODUCT DETAILS");
      ft.addToBackStack(null);
    } catch (Exception e) {
      Log.e(TAG, "callDetailsProduct: " + e.getMessage());
    }
  }

  public void callEditProduct(String idProducto) {
    //llamar al edit
    try {
      Intent mainIntent = new Intent(contexto, EditProductActivity.class);
      mainIntent.putExtra("ID_PRODUCT", idProducto);
      ProductsFragment.this.startActivity(mainIntent);
    } catch (Exception e) {
      Log.e(TAG, "callEditProduct: " + e.getMessage());
    }
  }

  public void callStockProduct(String idProducto) {
    //llamar al edit
    try {
      final FragmentTransaction ft = getFragmentManager().beginTransaction();
      ft.replace(R.id.frame, new ProductsStockFragment(idProducto));
      ft.commit();
      toolbar.setTitle("STOCK");
      ft.addToBackStack(null);
    } catch (Exception e) {
      Log.e(TAG, "callEditProduct: " + e.getMessage());
    }
  }

  @Override
  public void onPrepareOptionsMenu(Menu menu) {
    try {
      menu.findItem(R.id.menu_refresh_dashboard).setVisible(false);
      menu.findItem(R.id.menu_add_promotion).setVisible(false);
      menu.findItem(R.id.menu_edit_product).setVisible(false);
      super.onPrepareOptionsMenu(menu);
    } catch (Exception e) {
      Log.e(TAG, "onPrepareOptionsMenu: " + e.getMessage() );
    }
  }
}
