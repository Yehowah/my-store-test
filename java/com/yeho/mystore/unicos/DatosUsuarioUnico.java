package com.yeho.mystore.unicos;

import android.support.v7.widget.Toolbar;
import com.yeho.mystore.entidades.Products;
import com.yeho.mystore.entidades.Promotions;
import java.util.ArrayList;

/**
 * Created by yeho on 17/01/17.
 */

public class DatosUsuarioUnico {

  private static DatosUsuarioUnico instancia = null;
  private int pantallaActual = 0;
  Toolbar toolbar = null;
  ArrayList<Products> listaProductos = null;
  ArrayList<Promotions> listaPromotions = null;
  String idProductoEnPantalla = null;

  private DatosUsuarioUnico() {
  }

  public static DatosUsuarioUnico obtenerInstancia() {
    if (instancia == null) {
      instancia = new DatosUsuarioUnico();
    }
    return instancia;
  }


  public int getPantallaActual() {
    return pantallaActual;
  }

  public void setPantallaActual(int pantallaActual) {
    this.pantallaActual = pantallaActual;
  }

  public Toolbar getToolbar() {
    return toolbar;
  }

  public void setToolbar(Toolbar toolbar) {
    this.toolbar = toolbar;
  }

  public ArrayList<Products> getListaProductos() {
    return listaProductos;
  }

  public void setListaProductos(ArrayList<Products> listaProductos) {
    this.listaProductos = listaProductos;
  }

  public String getIdProductoEnPantalla() {
    return idProductoEnPantalla;
  }

  public void setIdProductoEnPantalla(String idProductoEnPantalla) {
    this.idProductoEnPantalla = idProductoEnPantalla;
  }

  public ArrayList<Promotions> getListaPromotions() {
    return listaPromotions;
  }

  public void setListaPromotions(ArrayList<Promotions> listaPromotions) {
    this.listaPromotions = listaPromotions;
  }
}
