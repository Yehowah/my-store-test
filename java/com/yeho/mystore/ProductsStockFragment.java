package com.yeho.mystore;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.GridView;
import com.yeho.mystore.adapters.AdapterStockProducts;
import com.yeho.mystore.entidades.Products;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductsStockFragment extends Fragment {

  private static final String TAG = "ProductsStockFragment";
  Context contexto = null;
  GridView gridStockProducts = null;
  String idProducto = null;

  public ProductsStockFragment() {
    // Required empty public constructor
  }

  public ProductsStockFragment(String idProducto) {
    this.idProducto = idProducto;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View v = null;
    try {

      contexto = getActivity();
      LayoutInflater gridInflater =
          (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      v = gridInflater.inflate(R.layout.fragment_products_stock, null);

      gridStockProducts = (GridView) v.findViewById(R.id.gridStockProducts);

      // toolbar = datosusuario.getToolbar();
      // ArrayList<Products> productsLista = new ArrayList<>();

      gridStockProducts.setAdapter(new AdapterStockProducts(contexto));
    } catch (Exception e) {
      Log.e(TAG, "onCreateView: " + e.getMessage());
    }

    return v;
  }

  @Override public void onPrepareOptionsMenu(Menu menu) {
    try {
      menu.findItem(R.id.menu_refresh_dashboard).setVisible(false);
      menu.findItem(R.id.menu_add_promotion).setVisible(false);
      menu.findItem(R.id.menu_edit_product).setVisible(false);
      super.onPrepareOptionsMenu(menu);
    } catch (Exception e) {
      Log.e(TAG, "onPrepareOptionsMenu: " + e.getMessage());
    }
  }

  @Override public void onPause() {
    super.onPause();
    actualizaProduto();
  }

  @Override public void onStop() {
    super.onStop();
  }

  public void actualizaProduto() {


   /* Products productoActualizado = new Products();
    productoActualizado.setName(etNameProduct.getText().toString());
    productoActualizado.setDescription(etDescriptionProduct.getText().toString());
    productoActualizado.setPrice(etPriceProduct.getText().toString());
    productoActualizado.setAvailable_date(etAvailableDate.getText().toString());
    productoActualizado.setShipping(etShipping.getText().toString());

    // datosUsuarioUnico.getListaProductos().get(Integer.valueOf(idProducto));
    ArrayList<Products> listaProductos = datosUsuarioUnico.getListaProductos() ;

    for ( Products p :listaProductos){
      if (p.getId().equals(idProducto)){
        productoActualizado.setId(p.getId());
        productoActualizado.setStock(p.getStock());
        productoActualizado.setImage(p.getImage());
        productoActualizado.setCategories(p.getCategories());
        listaProductos.set(Integer.valueOf(idProducto), productoActualizado);

        datosUsuarioUnico.setListaProductos(listaProductos);
        break;
      }
    }
*/
  }

}
