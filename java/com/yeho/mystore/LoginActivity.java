package com.yeho.mystore;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

  EditText etEmail = null;
  EditText etPassword = null;
  private static final String TAG = "LoginActivity";

  @Override protected void onCreate(Bundle savedInstanceState) {
    requestWindowFeature(Window.FEATURE_NO_TITLE);
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login);
    // Set up the login form.
    etEmail = (EditText) findViewById(R.id.etEmail);
    etPassword = (EditText) findViewById(R.id.etPassword);

    ImageView btnSignIn = (ImageView) findViewById(R.id.btnSignIn);
    btnSignIn.setOnClickListener(this);

  }

  private boolean isEmailValid(String email) {
    //TODO: Replace this with your own logic
    return email.contains("@");
  }

  private boolean isPasswordValid(String password) {
    //TODO: Replace this with your own logic
    return password.length() > 4;
  }

  @Override public void onClick(View view) {
    switch (view.getId()) {
      case R.id.btnSignIn: {
        try {
          validarCampos();
        } catch (Exception ex) {
          Log.e(TAG, "onClick: " + ex.getMessage());
        }
        break;
      }
    }
  }

  private void validarCampos() {
    boolean isOk = true;
  //  if (isEmailValid(etEmail.getText().toString())){
   //   if (isPasswordValid(etPassword.getText().toString())){
        ingresar();
   /*   }else{
        isOk = false;
      }
    }else{
      isOk = false;
    }
*/
    if(!isOk) {
      AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
      alertDialogBuilder.setTitle("MyStore")
          .setMessage("Datos invalidos")
          .setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
              etEmail.requestFocus();
            }
          })
          .show();
    }
  }

  private void ingresar() {
    Intent mainIntent = new Intent(LoginActivity.this, MainActivity.class);
    LoginActivity.this.startActivity(mainIntent);
  }
}

