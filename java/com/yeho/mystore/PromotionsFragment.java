package com.yeho.mystore;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import com.yeho.mystore.adapters.AdapterPromotions;
import com.yeho.mystore.entidades.Promotions;
import com.yeho.mystore.unicos.DatosUsuarioUnico;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PromotionsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PromotionsFragment extends Fragment {
  // TODO: Rename parameter arguments, choose names that match
  // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";
  GridView gridPromotions = null;
  Context contexto = null;
  private static final String TAG = "PromotionsFragment";
  DatosUsuarioUnico datosusuario = DatosUsuarioUnico.obtenerInstancia();
  Toolbar toolbar = null;

  // TODO: Rename and change types of parameters
  private String mParam1;
  private String mParam2;

  public PromotionsFragment() {
    // Required empty public constructor
  }

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   *
   * @param param1 Parameter 1.
   * @param param2 Parameter 2.
   * @return A new instance of fragment PromotionsFragment.
   */
  // TODO: Rename and change types and number of parameters
  public static PromotionsFragment newInstance(String param1, String param2) {
    PromotionsFragment fragment = new PromotionsFragment();
    Bundle args = new Bundle();
    args.putString(ARG_PARAM1, param1);
    args.putString(ARG_PARAM2, param2);
    fragment.setArguments(args);
    return fragment;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
      mParam1 = getArguments().getString(ARG_PARAM1);
      mParam2 = getArguments().getString(ARG_PARAM2);
    }
    setHasOptionsMenu(true);
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View v= null;
    try {
      contexto = getActivity();
      Bundle args = getArguments();

      toolbar = datosusuario.getToolbar();
      LayoutInflater gridInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      v = gridInflater.inflate(R.layout.fragment_promotions, null);

      gridPromotions = (GridView) v.findViewById(R.id.gridPromotions);

      if (datosusuario.getListaPromotions() == null) {
        agregaPromocionesDummy();
      }

      gridPromotions.setAdapter(new AdapterPromotions(contexto, PromotionsFragment.this));

    } catch (Exception e) {
      Log.e(TAG, "onCreateView: " + e.getMessage() );
    }

    return v;
  }

  @Override public void onResume() {
    super.onResume();
    if (datosusuario.getListaPromotions() != null) {
      gridPromotions.setAdapter(new AdapterPromotions(contexto, PromotionsFragment.this));
    }
  }

  private void agregaPromocionesDummy() {

    ArrayList<Promotions> promocionesLista = new ArrayList<>();

    Promotions promocion = new Promotions();
    promocion.setId("0");
    promocion.setName("Free sunglases for true hero ambassador steve");
    promocion.setCode("RevoProSteve");
    promocion.setUses("20");
    promocion.setExpiration("Oct 30 2017");
    promocion.setDescription("cool description - Free sunglases for true hero ambassador steve");
    promocion.setUsage_limit("35");
    Promotions promocion2 = new Promotions();
    promocion.setId("1");
    promocion2.setName("20% off full price orders");
    promocion2.setCode("ShopeSale");
    promocion2.setUses("12");
    promocion2.setExpiration("Jun 30, 2017");
    promocion2.setDescription("cool description - 20% off full price orders");
    promocion2.setUsage_limit("20");
    Promotions promocion3 = new Promotions();
    promocion.setId("2");
    promocion3.setName("Free sunglases for true hero ambassador another steve");
    promocion3.setCode("RevoProAnotherSteve");
    promocion3.setUses("25");
    promocion3.setExpiration("May 30, 2017");
    promocion3.setDescription(
        "super cool description - Free sunglases for true hero ambassador another steve");
    promocion3.setUsage_limit("35");

    promocionesLista.add(promocion);
    promocionesLista.add(promocion2);
    promocionesLista.add(promocion3);

    datosusuario.setListaPromotions(promocionesLista);
  }

  @Override
  public void onPrepareOptionsMenu(Menu menu) {
    try {
      menu.findItem(R.id.menu_refresh_dashboard).setVisible(false);
      menu.findItem(R.id.menu_add_promotion).setVisible(true);
      menu.findItem(R.id.menu_edit_product).setVisible(false);
      super.onPrepareOptionsMenu(menu);
    } catch (Exception e) {
      Log.e(TAG, "onPrepareOptionsMenu: " + e.getMessage() );
    }
  }

  public void callDetailsPromo(String idPromo) {
    //llamar al details
    try {
    /*  final FragmentTransaction ft = getFragmentManager().beginTransaction();
      ft.replace(R.id.frame, new ProductDetailFragment(idProducto));
      ft.commit();
      toolbar.setTitle("PRODUCT DETAILS");
      ft.addToBackStack(null);
    */
    } catch (Exception e) {
      Log.e(TAG, "callDetailsPromo: " + e.getMessage());
    }
  }

  public void callEditPomo(String idPromo) {
    //llamar al edit
    try {
      /*Intent mainIntent = new Intent(contexto, EditProductActivity.class);
      mainIntent.putExtra("ID_PRODUCT", idProducto);
      PromotionsFragment.this.startActivity(mainIntent);
    */
    } catch (Exception e) {
      Log.e(TAG, "callEditPomo: " + e.getMessage());
    }
  }

}
