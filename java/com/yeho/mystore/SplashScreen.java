package com.yeho.mystore;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;

/**
 * Created by yeho on 17/01/17.
 */
public class SplashScreen extends AppCompatActivity {

  private final int SPLASH_DISPLAY_LENGTH = 3000;
  private static final String TAG = "SplashScreen";
  Context contexto = null;

  @Override protected void onCreate(Bundle savedInstanceState) {
    requestWindowFeature(Window.FEATURE_NO_TITLE);
    super.onCreate(savedInstanceState);
    try {
      setContentView(R.layout.activity_splash_screen);

      contexto = this;

      // call Login Activity
      new Handler().postDelayed(new Runnable() {
        @Override public void run() {
                /* Create an Intent that will start the Menu-Activity. */
          Intent mainIntent = new Intent(SplashScreen.this, DomainAcces.class);
          SplashScreen.this.startActivity(mainIntent);
          SplashScreen.this.finish();
        }
      }, SPLASH_DISPLAY_LENGTH);
    } catch (Exception ex) {
      Log.e(TAG, "onCreate: " + ex.getMessage());
    }
  }
}
