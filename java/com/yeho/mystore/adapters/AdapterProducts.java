package com.yeho.mystore.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.yeho.mystore.ProductsFragment;
import com.yeho.mystore.R;
import com.yeho.mystore.entidades.Products;
import com.yeho.mystore.unicos.DatosUsuarioUnico;
import java.util.ArrayList;

/**
 * Created by yeho on 18/01/17.
 */
public class AdapterProducts extends BaseAdapter {
  private Context contexto;
  private ArrayList<Products> products = null;
  ProductsFragment fragment = null;
  DatosUsuarioUnico datosusuario = DatosUsuarioUnico.obtenerInstancia();

  Typeface Lato_Light = null;
  Typeface Lato_Regular = null;
  private static final String TAG = "AdapterProducts";

  public AdapterProducts(Context contexto,
      ProductsFragment fragment) {
    this.contexto = contexto;
    this.products = datosusuario.getListaProductos();
    this.fragment = fragment;
    Lato_Light = Typeface.createFromAsset(contexto.getAssets(), "fonts/Lato-Light.ttf");
    Lato_Regular = Typeface.createFromAsset(contexto.getAssets(), "fonts/Lato-Regular.ttf");
  }

  public View getView(final int position, View convertView, ViewGroup parent) {

    View vista = null;
    try {
      if (convertView == null) {  // if it's not recycled, initialize some attributes
        LayoutInflater inflater =
            (LayoutInflater) contexto.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        vista = inflater.inflate(R.layout.item_products, parent, false);
      } else {
        vista = (View) convertView;
      }

      ImageView imgProduct = (ImageView) vista.findViewById(R.id.imgProduct);
      TextView tvNameProduct = (TextView) vista.findViewById(R.id.tvNameProduct);
      TextView tvPrice = (TextView) vista.findViewById(R.id.tvPrice);
      TextView tvStock = (TextView) vista.findViewById(R.id.tvStock);
      TextView price = (TextView) vista.findViewById(R.id.price);
      TextView stock = (TextView) vista.findViewById(R.id.stock);

      LinearLayout lyDetailsProduct = (LinearLayout) vista.findViewById(R.id.lyDetailsProduct);
      LinearLayout lyEditProduct = (LinearLayout) vista.findViewById(R.id.lyEditProduct);
      LinearLayout lyStockProduct = (LinearLayout) vista.findViewById(R.id.lyStockProduct);

      lyDetailsProduct.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View view) {
          fragment.callDetailsProduct(products.get(position).getId());
        }
      });

      lyEditProduct.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View view) {
          fragment.callEditProduct(products.get(position).getId());
        }
      });

      lyStockProduct.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View view) {
          fragment.callStockProduct(products.get(position).getId());
        }
      });

      tvNameProduct.setTypeface(Lato_Regular);
      tvPrice.setTypeface(Lato_Light);
      tvStock.setTypeface(Lato_Light);
      price.setTypeface(Lato_Light);
      stock.setTypeface(Lato_Light);

      Products producto = datosusuario.getListaProductos().get(position);
      tvNameProduct.setText(producto.getName());
      tvPrice.setText(producto.getPrice());
      tvStock.setText(producto.getStock());

      Picasso.with(contexto).load(obtenReferenciaImagen(producto.getImage())).into(imgProduct);

    } catch (Exception ex) {
      Log.e(TAG, "getView: error " + ex.getMessage());
    }
    return vista;
  }

  private int obtenReferenciaImagen(String image) {
    int referencia = 0;

    if (image.equals("campbell_picture_1")) {
      referencia = R.drawable.campbell_picture_1;
    } else if (image.equals("evans_picture_1")) {
      referencia = R.drawable.evans_picture_1;
    } else if (image.equals("morgan_picture_1")) {
      referencia = R.drawable.morgan_picture_1;
    } else if (image.equals("robinson_picture_1")) {
      referencia = R.drawable.robinson_picture_1;
    }
    return referencia;
  }

  @Override public int getCount() {
    return products.size();
  }

  @Override public Object getItem(int position) {
    return null;
  }

  @Override public long getItemId(int position) {
    return 0;
  }
}
