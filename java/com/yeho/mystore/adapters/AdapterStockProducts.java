package com.yeho.mystore.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;
import com.yeho.mystore.ProductsFragment;
import com.yeho.mystore.R;
import com.yeho.mystore.entidades.Products;
import com.yeho.mystore.unicos.DatosUsuarioUnico;
import java.util.ArrayList;

/**
 * Created by yeho on 18/01/17.
 */
public class AdapterStockProducts extends BaseAdapter {
  private Context contexto;
  private ArrayList<Products> products = null;
  ProductsFragment fragment = null;
  DatosUsuarioUnico datosusuario = DatosUsuarioUnico.obtenerInstancia();
  EditText etCantStock = null;
  Products producto = null;

  Typeface Lato_Light = null;
  Typeface Lato_Regular = null;
  private static final String TAG = "AdapterStockProducts";

  public AdapterStockProducts(Context contexto) {
    this.contexto = contexto;
    this.products = datosusuario.getListaProductos();
    this.fragment = fragment;
    Lato_Light = Typeface.createFromAsset(contexto.getAssets(), "fonts/Lato-Light.ttf");
    Lato_Regular = Typeface.createFromAsset(contexto.getAssets(), "fonts/Lato-Regular.ttf");
  }

  public View getView(int position, View convertView, ViewGroup parent) {

    View vista = null;
    try {

      if (convertView == null) {  // if it's not recycled, initialize some attributes
        LayoutInflater inflater =
            (LayoutInflater) contexto.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        vista = inflater.inflate(R.layout.item_stock_products, parent, false);
      } else {
        vista = (View) convertView;
      }

      producto = datosusuario.getListaProductos().get(position);
      TextView tvInStock = (TextView) vista.findViewById(R.id.tvInStock);
      tvInStock.setText("In Stock: " + producto.getStock());

      etCantStock = (EditText) vista.findViewById(R.id.etCantStock);
      etCantStock.setText(datosusuario.getListaProductos().get(position).getStock());

      etCantStock.setOnFocusChangeListener(new View.OnFocusChangeListener() {
        @Override public void onFocusChange(View v, boolean hasFocus) {
          if (!hasFocus) {
            String stock = etCantStock.getText().toString();
            datosusuario.getListaProductos().get(Integer.valueOf(producto.getId())).setStock(stock);
          }
        }
      });


      //etCantStock.setInputType(InputType.TYPE_CLASS_NUMBER);
      etCantStock.setInputType(
          InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
      etCantStock.setTransformationMethod(new NumericKeyBoardTransformationMethod());
     /* ImageView imgProduct = (ImageView) vista.findViewById(R.id.imgProduct);
      TextView tvNameProduct = (TextView) vista.findViewById(R.id.tvNameProduct);
      TextView tvPrice = (TextView) vista.findViewById(R.id.tvPrice);
      TextView tvStock = (TextView) vista.findViewById(R.id.tvStock);

      LinearLayout lyDetailsProduct = (LinearLayout) vista.findViewById(R.id.lyDetailsProduct);
      LinearLayout lyEditProduct = (LinearLayout) vista.findViewById(R.id.lyEditProduct);
      LinearLayout lyStockProduct = (LinearLayout) vista.findViewById(R.id.lyStockProduct);


      tvNameProduct.setTypeface(Lato_Regular);
      tvPrice.setTypeface(Lato_Light);
      tvStock.setTypeface(Lato_Light);
      */
    } catch (Exception ex) {
      Log.e(TAG, "getView: error " + ex.getMessage());
    }
    return vista;
  }

  private class NumericKeyBoardTransformationMethod extends PasswordTransformationMethod {
    @Override public CharSequence getTransformation(CharSequence source, View view) {
      return source;
    }
  }

  @Override public int getCount() {
    return products.size();
  }

  @Override public Object getItem(int position) {
    return null;
  }

  @Override public long getItemId(int position) {
    return 0;
  }
}
