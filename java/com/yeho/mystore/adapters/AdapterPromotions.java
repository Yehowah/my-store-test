package com.yeho.mystore.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.yeho.mystore.PromotionsFragment;
import com.yeho.mystore.R;
import com.yeho.mystore.entidades.Promotions;
import com.yeho.mystore.unicos.DatosUsuarioUnico;
import java.util.ArrayList;

/**
 * Created by yeho on 17/01/17.
 */
public class AdapterPromotions extends BaseAdapter {
	private Context contexto;
	private ArrayList<Promotions> promotions = null;
	DatosUsuarioUnico datosusuario = DatosUsuarioUnico.obtenerInstancia();
	PromotionsFragment fragment = null;
	Promotions promo = null;

	Typeface Lato_Light = null;
	Typeface Lato_Regular = null;
	private static final String TAG = "AdapterPromotions";

	public AdapterPromotions(Context contexto, PromotionsFragment fragment) {
		this.contexto = contexto;
		this.fragment = fragment;
		this.promotions = datosusuario.getListaPromotions();
		Lato_Light = Typeface.createFromAsset(contexto.getAssets(), "fonts/Lato-Light.ttf");
		Lato_Regular = Typeface.createFromAsset(contexto.getAssets(), "fonts/Lato-Regular.ttf");
	}


	public View getView(int position, View convertView, ViewGroup parent) {

		View vista = null;
		try {
            LayoutInflater inflater = (LayoutInflater) contexto.getSystemService(     Context.LAYOUT_INFLATER_SERVICE );
            vista = inflater.inflate(R.layout.item_promotions, parent, false);

			TextView code = (TextView) vista.findViewById(R.id.code);
			TextView use = (TextView) vista.findViewById(R.id.use);
			TextView expiration = (TextView) vista.findViewById(R.id.expiration);
			TextView tvNamePromotion = (TextView) vista.findViewById(R.id.tvNamePromotion);
			TextView tvCode = (TextView) vista.findViewById(R.id.tvCode);
			TextView tvUse = (TextView) vista.findViewById(R.id.tvUse);
			TextView tvExp = (TextView) vista.findViewById(R.id.tvExp);
			ImageView imgDetailsPromo = (ImageView) vista.findViewById(R.id.imgDetailsPromo);
			ImageView imgEditPromo = (ImageView) vista.findViewById(R.id.imgEditPromo);

			tvNamePromotion.setTypeface(Lato_Regular);
			tvCode.setTypeface(Lato_Light);
			tvUse.setTypeface(Lato_Light);
			tvExp.setTypeface(Lato_Light);
			code.setTypeface(Lato_Light);
			use.setTypeface(Lato_Light);
			expiration.setTypeface(Lato_Light);

			promo = promotions.get(position);

			tvNamePromotion.setText(promo.getName());
			tvCode.setText(promo.getCode());
			tvUse.setText(
					(promo.getUses() != null) ? promo.getUses() : "0" + " left of " + promo.getUsage_limit());
			tvExp.setText(promo.getExpiration());

			imgDetailsPromo.setOnClickListener(new View.OnClickListener() {
				@Override public void onClick(View view) {
					fragment.callDetailsPromo(promo.getId());
				}
			});
			imgEditPromo.setOnClickListener(new View.OnClickListener() {
				@Override public void onClick(View view) {
					fragment.callEditPomo(promo.getId());
				}
			});




		}catch(Exception ex){
			Log.e(TAG, "getView: error " +ex.getMessage() );
		}
		return vista;
	}




	@Override
	public int getCount() {
		return promotions.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}




}
