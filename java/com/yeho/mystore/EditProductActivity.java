package com.yeho.mystore;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;
import com.yeho.mystore.entidades.Products;
import com.yeho.mystore.unicos.DatosUsuarioUnico;
import java.util.ArrayList;

public class EditProductActivity extends AppCompatActivity {

  EditText etNameProduct = null;
  EditText etDescriptionProduct = null;
  EditText etPriceProduct = null;
  EditText etAvailableDate = null;
  EditText etShipping = null;
  ImageView imgProducto = null;
  DatosUsuarioUnico datosUsuarioUnico = DatosUsuarioUnico.obtenerInstancia();
  String idProducto = null;
  Context contexto = null;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_edit_product);
    contexto = this;
    idProducto = getIntent().getStringExtra("ID_PRODUCT");

    imgProducto = (ImageView) findViewById(R.id.imgProducto);
    etNameProduct = (EditText) findViewById(R.id.etNameProduct);
    etDescriptionProduct = (EditText) findViewById(R.id.etDescriptionProduct);
    etPriceProduct = (EditText) findViewById(R.id.etPriceProduct);
    etAvailableDate = (EditText) findViewById(R.id.etAvailableDate);
    etShipping = (EditText) findViewById(R.id.etShipping);

    Products producto = datosUsuarioUnico.getListaProductos().get(Integer.valueOf(idProducto));
    etNameProduct.setText(producto.getName());
    etDescriptionProduct.setText(producto.getDescription());
    etPriceProduct.setText(producto.getPrice());
    etAvailableDate.setText(producto.getAvailable_date());
    etShipping.setText(producto.getShipping());

    Picasso.with(contexto).load(obtenReferenciaImagen(producto.getImage())).into(imgProducto);


  }

  private int obtenReferenciaImagen(String image) {
    int referencia = 0;

    if (image.equals("campbell_picture_1")) {
      referencia = R.drawable.campbell_picture_1;
    } else if (image.equals("evans_picture_1")) {
      referencia = R.drawable.evans_picture_1;
    } else if (image.equals("morgan_picture_1")) {
      referencia = R.drawable.morgan_picture_1;
    } else if (image.equals("robinson_picture_1")) {
      referencia = R.drawable.robinson_picture_1;
    }
    return referencia;
  }

  @Override public void onBackPressed() {
    actualizaProduto();
    if (getFragmentManager().getBackStackEntryCount() > 0) {
      getFragmentManager().popBackStack();
    } else {
      super.onBackPressed();
    }
  }

  public void actualizaProduto() {

    Products productoActualizado = new Products();
    productoActualizado.setName(etNameProduct.getText().toString());
    productoActualizado.setDescription(etDescriptionProduct.getText().toString());
    productoActualizado.setPrice(etPriceProduct.getText().toString());
    productoActualizado.setAvailable_date(etAvailableDate.getText().toString());
    productoActualizado.setShipping(etShipping.getText().toString());

    // datosUsuarioUnico.getListaProductos().get(Integer.valueOf(idProducto));
    ArrayList<Products> listaProductos = datosUsuarioUnico.getListaProductos();

    for (Products p : listaProductos) {
      if (p.getId().equals(idProducto)) {
        productoActualizado.setId(p.getId());
        productoActualizado.setStock(p.getStock());
        productoActualizado.setImage(p.getImage());
        productoActualizado.setCategories(p.getCategories());
        listaProductos.set(Integer.valueOf(idProducto), productoActualizado);

        datosUsuarioUnico.setListaProductos(listaProductos);
        break;
      }
    }
  }

}
