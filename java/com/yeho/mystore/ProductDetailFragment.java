package com.yeho.mystore;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.yeho.mystore.entidades.Products;
import com.yeho.mystore.unicos.DatosUsuarioUnico;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductDetailFragment extends Fragment {

  private static final String TAG = "ProductDetailFragment";
  String idProducto = null;
  Context contexto = null;
  DatosUsuarioUnico datosusuario = DatosUsuarioUnico.obtenerInstancia();
  ImageView imgProducto = null;
  TextView tvNameProduct = null;
  TextView tvDescription = null;
  TextView tvPrice = null;
  TextView tvAvailable = null;
  TextView tvShipping = null;
  TextView precio = null;
  TextView descripcion = null;
  TextView available = null;
  TextView shipping = null;
  public Typeface Lato_Light = null;
  public Typeface Lato_Regular = null;

  public ProductDetailFragment() {
    // Required empty public constructor
  }

  @SuppressLint("ValidFragment") public ProductDetailFragment(String idProducto) {
    this.idProducto = idProducto;

  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
  }

  @Override public void onResume() {
    super.onResume();

    Products producto = datosusuario.getListaProductos().get(Integer.valueOf(idProducto));
    tvNameProduct.setText(producto.getName());
    tvDescription.setText(producto.getDescription());
    tvPrice.setText(producto.getPrice());
    tvAvailable.setText(producto.getAvailable_date());
    tvShipping.setText(producto.getShipping());
    Picasso.with(contexto).load(obtenReferenciaImagen(producto.getImage())).into(imgProducto);
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    // return inflater.inflate(R.layout.fragment_product_detail, container, false);

    try {
      Lato_Light = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Lato-Light.ttf");
      Lato_Regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Lato-Regular.ttf");
    } catch (Exception e) {
      Log.e(TAG, "onCreateView: " + e.getMessage());
    }
    datosusuario.setIdProductoEnPantalla(idProducto);
    View v = null;
    try {
      contexto = getActivity();
      LayoutInflater gridInflater =
          (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      v = gridInflater.inflate(R.layout.fragment_product_detail, null);

      Products producto = datosusuario.getListaProductos().get(Integer.valueOf(idProducto));

      imgProducto = (ImageView) v.findViewById(R.id.imgProducto);
      tvNameProduct = (TextView) v.findViewById(R.id.tvNameProduct);
      tvDescription = (TextView) v.findViewById(R.id.tvDescription);
      tvPrice = (TextView) v.findViewById(R.id.tvPrice);
      tvAvailable = (TextView) v.findViewById(R.id.tvAvailable);
      tvShipping = (TextView) v.findViewById(R.id.tvShipping);
      descripcion = (TextView) v.findViewById(R.id.descripcion);
      precio = (TextView) v.findViewById(R.id.precio);
      available = (TextView) v.findViewById(R.id.available);
      shipping = (TextView) v.findViewById(R.id.shipping);


      tvNameProduct.setText(producto.getName());
      tvDescription.setText(producto.getDescription());
      tvPrice.setText(producto.getPrice());
      tvAvailable.setText(producto.getAvailable_date());
      tvShipping.setText(producto.getShipping());

      try {
        tvNameProduct.setTypeface(Lato_Regular);
        tvDescription.setTypeface(Lato_Regular);
        tvPrice.setTypeface(Lato_Regular);
        tvAvailable.setTypeface(Lato_Regular);
        tvShipping.setTypeface(Lato_Regular);

        descripcion.setTypeface(Lato_Light);
        precio.setTypeface(Lato_Light);
        available.setTypeface(Lato_Light);
        shipping.setTypeface(Lato_Light);
      } catch (Exception e) {
        Log.e(TAG, "onCreateView: " + e.getMessage());
      }

      Picasso.with(contexto).load(obtenReferenciaImagen(producto.getImage())).into(imgProducto);
    } catch (Exception ex) {
      Log.e(TAG, "onCreateView: " + ex.getMessage());
    }

    return v;
  }

  private int obtenReferenciaImagen(String image) {
    int referencia = 0;

    if (image.equals("campbell_picture_1")) {
      referencia = R.drawable.campbell_picture_1;
    } else if (image.equals("evans_picture_1")) {
      referencia = R.drawable.evans_picture_1;
    } else if (image.equals("morgan_picture_1")) {
      referencia = R.drawable.morgan_picture_1;
    } else if (image.equals("robinson_picture_1")) {
      referencia = R.drawable.robinson_picture_1;
    }
    return referencia;
  }

  @Override public void onPrepareOptionsMenu(Menu menu) {
    try {
      menu.findItem(R.id.menu_refresh_dashboard).setVisible(false);
      menu.findItem(R.id.menu_add_promotion).setVisible(false);
      super.onPrepareOptionsMenu(menu);
    } catch (Exception e) {
      Log.e(TAG, "onPrepareOptionsMenu: " + e.getMessage());
    }
  }
}
