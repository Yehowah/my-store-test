package com.yeho.mystore;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import com.yeho.mystore.unicos.DatosUsuarioUnico;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

  Toolbar toolbar = null;
  DatosUsuarioUnico datosusuario = DatosUsuarioUnico.obtenerInstancia();
  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    toolbar.setTitleTextColor(getResources().getColor(R.color.fountain));

    datosusuario.setToolbar(toolbar);
    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    ActionBarDrawerToggle toggle =
        new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
    drawer.setDrawerListener(toggle);
    toggle.syncState();

    NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
    navigationView.setNavigationItemSelectedListener(this);
    navigationView.setItemIconTintList(null);

    try {
      DashboardFragment dashboardFragment = new DashboardFragment();
      android.support.v4.app.FragmentTransaction dashboardFragmentTransaction =
          getSupportFragmentManager().beginTransaction();
      dashboardFragmentTransaction.replace(R.id.frame, dashboardFragment);
      dashboardFragmentTransaction.commit();
    } catch (Exception e) {
      Log.e("YEHO", "inicializando pantalla: " + e.getMessage());
    }
  }

  @Override public void onBackPressed() {
   /* DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    if (drawer.isDrawerOpen(GravityCompat.START)) {
      drawer.closeDrawer(GravityCompat.START);
    } else {
      super.onBackPressed();
    }*/

    if (getFragmentManager().getBackStackEntryCount() > 0) {
      getFragmentManager().popBackStack();
    } else {
      super.onBackPressed();
    }
  }

  @SuppressWarnings("StatementWithEmptyBody") @Override public boolean onNavigationItemSelected(MenuItem item) {
    // Handle navigation view item clicks here.
    int id = item.getItemId();

    if (id == R.id.nav_dashboard) {
      try {
        DashboardFragment dashboardFragment = new DashboardFragment();
        android.support.v4.app.FragmentTransaction dashboardFragmentTransaction =
            getSupportFragmentManager().beginTransaction();
        dashboardFragmentTransaction.replace(R.id.frame, dashboardFragment);
        dashboardFragmentTransaction.commit();
        toolbar.setTitle("DASHBOARD");
      } catch (Exception e) {
        Log.e("YEHO", "onNavigationItemSelected: " + e.getMessage());
      }
    } else if (id == R.id.nav_orders) {
      OrdersFragment ordersFragment = new OrdersFragment();
      android.support.v4.app.FragmentTransaction ordersFragmentTransaction =
          getSupportFragmentManager().beginTransaction();
      ordersFragmentTransaction.replace(R.id.frame, ordersFragment);
      ordersFragmentTransaction.commit();
      toolbar.setTitle("ORDERS");
    } else if (id == R.id.nav_products) {
      ProductsFragment productsFragment = new ProductsFragment();
      android.support.v4.app.FragmentTransaction productsFragmentTransaction =
          getSupportFragmentManager().beginTransaction();
      productsFragmentTransaction.replace(R.id.frame, productsFragment);
      productsFragmentTransaction.commit();
      toolbar.setTitle("PRODUCTS");
    } else if (id == R.id.nav_promotions) {
      PromotionsFragment promotionsFragment = new PromotionsFragment();
      android.support.v4.app.FragmentTransaction promotionsFragmentTransaction =
          getSupportFragmentManager().beginTransaction();
      promotionsFragmentTransaction.replace(R.id.frame, promotionsFragment);
      promotionsFragmentTransaction.commit();
      toolbar.setTitle("PROMOTIONS");

    } else if (id == R.id.nav_settings) {
      SettingsFragment settingsFragment = new SettingsFragment();
      android.support.v4.app.FragmentTransaction settingsFragmentTransaction =
          getSupportFragmentManager().beginTransaction();
      settingsFragmentTransaction.replace(R.id.frame, settingsFragment);
      settingsFragmentTransaction.commit();
      toolbar.setTitle("SETTINGS");
    } else if (id == R.id.nav_signout) {
      this.finish();

    }

    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    drawer.closeDrawer(GravityCompat.START);
    datosusuario.setPantallaActual(id); // se mantiene la referencia a la pantalla actual para modificar el toolbar

    return true;
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    menu.clear();
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_toolbar, menu);

    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.menu_add_promotion:
        // EITHER CALL THE METHOD HERE OR DO THE FUNCTION DIRECTLY
        addPromotion();

        return true;
      case R.id.menu_edit_product:
        // EITHER CALL THE METHOD HERE OR DO THE FUNCTION DIRECTLY
        editProduct();

        return true;

      default:
        return super.onOptionsItemSelected(item);
    }
  }

  private void addPromotion() {
    Intent mainIntent = new Intent(MainActivity.this, NewPromotion.class);
    MainActivity.this.startActivity(mainIntent);
  }

  private void editProduct() {
    Intent mainIntent = new Intent(MainActivity.this, EditProductActivity.class);
    mainIntent.putExtra("ID_PRODUCT", datosusuario.getIdProductoEnPantalla());
    MainActivity.this.startActivity(mainIntent);
  }
}