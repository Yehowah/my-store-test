package com.yeho.mystore;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

public class DomainAcces extends AppCompatActivity implements View.OnClickListener {

  private static final String TAG = "DomainAcces";

  @Override protected void onCreate(Bundle savedInstanceState) {
    requestWindowFeature(Window.FEATURE_NO_TITLE);
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_domain_acces);

    ImageView btnAccess = (ImageView) findViewById(R.id.btnAccess);
    btnAccess.setOnClickListener(this);
  }

  @Override public void onClick(View view) {
    switch (view.getId()) {
      case R.id.btnAccess: {
        try {
          irASignIn();
        } catch (Exception ex) {
          Log.e(TAG, "onClick: " + ex.getMessage());
        }
        break;
      }
    }
  }

  private void irASignIn() {
    Intent mainIntent = new Intent(DomainAcces.this, LoginActivity.class);
    DomainAcces.this.startActivity(mainIntent);
  }
}
