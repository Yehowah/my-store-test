package com.yeho.mystore;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * A simple {@link Fragment} subclass.
 */
public class PromoDetailsFragment extends Fragment {

  String idProducto = null;

  public PromoDetailsFragment() {
    // Required empty public constructor
  }

  @SuppressLint("ValidFragment") public PromoDetailsFragment(String idProducto) {
    this.idProducto = idProducto;
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    return inflater.inflate(R.layout.fragment_promo_details, container, false);
  }
}
