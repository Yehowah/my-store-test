package com.yeho.mystore;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment {

  private static final String TAG = "SettingsFragment";

  public SettingsFragment() {
    // Required empty public constructor
  }

  @Override public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    return inflater.inflate(R.layout.fragment_settings, container, false);
  }

  @Override public void onPrepareOptionsMenu(Menu menu) {
    try {
      menu.findItem(R.id.menu_add_promotion).setVisible(false);
      menu.findItem(R.id.menu_edit_product).setVisible(false);
      menu.findItem(R.id.menu_refresh_dashboard).setVisible(false);
      super.onPrepareOptionsMenu(menu);
    } catch (Exception e) {
      Log.e(TAG, "onPrepareOptionsMenu: " + e.getMessage());
    }
  }
}
