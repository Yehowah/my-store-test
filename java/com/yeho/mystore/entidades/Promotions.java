package com.yeho.mystore.entidades;

/**
 * Created by yeho on 17/01/17.
 */

public class Promotions {
  private String id = null;
  private String name = null;
  private String code = null;
  private String uses = null;
  private String expiration = null;
  private String path = null;
  private String description = null;
  private String category = null;
  private String usage_limit = null;
  private String starts_at = null;
  private String expires_at = null;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getUses() {
    return uses;
  }

  public void setUses(String uses) {
    this.uses = uses;
  }

  public String getExpiration() {
    return expiration;
  }

  public void setExpiration(String expiration) {
    this.expiration = expiration;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public String getUsage_limit() {
    return usage_limit;
  }

  public void setUsage_limit(String usage_limit) {
    this.usage_limit = usage_limit;
  }

  public String getStarts_at() {
    return starts_at;
  }

  public void setStarts_at(String starts_at) {
    this.starts_at = starts_at;
  }

  public String getExpires_at() {
    return expires_at;
  }

  public void setExpires_at(String expires_at) {
    this.expires_at = expires_at;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }
}
