package com.yeho.mystore.entidades;

import java.util.ArrayList;

/**
 * Created by yeho on 18/01/17.
 */

public class Products {
  private String id = null;
  private String name = null;
  private String description = null;
  private String price = null;
  private String available_date = null;
  private String shipping = null;
  private ArrayList<String> categories = null;
  private String image = null;
  private String stock = null;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getPrice() {
    return price;
  }

  public void setPrice(String price) {
    this.price = price;
  }

  public String getAvailable_date() {
    return available_date;
  }

  public void setAvailable_date(String available_date) {
    this.available_date = available_date;
  }

  public String getShipping() {
    return shipping;
  }

  public void setShipping(String shipping) {
    this.shipping = shipping;
  }

  public ArrayList<String> getCategories() {
    return categories;
  }

  public void setCategories(ArrayList<String> categories) {
    this.categories = categories;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public String getStock() {
    return stock;
  }

  public void setStock(String stock) {
    this.stock = stock;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }
}
