package com.yeho.mystore;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.widget.EditText;
import com.yeho.mystore.entidades.Promotions;
import com.yeho.mystore.unicos.DatosUsuarioUnico;
import java.util.ArrayList;

public class NewPromotion extends AppCompatActivity {

  EditText etNamePromotion = null;
  EditText etCodePromotion = null;
  EditText etNumberCoupon = null;
  EditText etDatePromotion = null;
  EditText etDescriptionPromotion = null;
  DatosUsuarioUnico datosUsuarioUnico = DatosUsuarioUnico.obtenerInstancia();

  @Override protected void onCreate(Bundle savedInstanceState) {
    requestWindowFeature(Window.FEATURE_NO_TITLE);
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_new_promotion);

    etNamePromotion = (EditText) findViewById(R.id.etNamePromotion);
    etCodePromotion = (EditText) findViewById(R.id.etCodePromotion);
    etNumberCoupon = (EditText) findViewById(R.id.etNumberCoupon);
    etDatePromotion = (EditText) findViewById(R.id.etDatePromotion);
    etDescriptionPromotion = (EditText) findViewById(R.id.etDescriptionPromotion);
  }

  @Override public void onBackPressed() {
    agregaNuevaPromo();
    if (getFragmentManager().getBackStackEntryCount() > 0) {
      getFragmentManager().popBackStack();
    } else {
      super.onBackPressed();
    }
  }

  public void agregaNuevaPromo() {

    boolean isOk = true;
    Promotions nuevaPromocion = new Promotions();
    if (etNamePromotion.getText().toString().length() == 0) {
      isOk = false;
    }
    if (etCodePromotion.getText().toString().length() == 0) {
      isOk = false;
    }
    if (etNumberCoupon.getText().toString().length() == 0) {
      isOk = false;
    }
    if (etDatePromotion.getText().toString().length() == 0) {
      isOk = false;
    }
    if (etDescriptionPromotion.getText().toString().length() == 0) {
      isOk = false;
    }
    if (isOk) {
      nuevaPromocion.setName(etNamePromotion.getText().toString());
      nuevaPromocion.setCode(etCodePromotion.getText().toString());
      nuevaPromocion.setUsage_limit(etNumberCoupon.getText().toString());
      nuevaPromocion.setExpiration(etDatePromotion.getText().toString());
      nuevaPromocion.setDescription(etDescriptionPromotion.getText().toString());

      ArrayList<Promotions> listaPromo = datosUsuarioUnico.getListaPromotions();
      listaPromo.add(nuevaPromocion);

      datosUsuarioUnico.setListaPromotions(listaPromo);
    }
  }
}
